<?php
/**
 * @file
 *
 * @author Gbindia -http://www.gbindia.in - twitter.com/ggbhat
 * @author Ulf Henrich - twitter.com/ulfhenrich - http://www.ulf-henrich.de
 */

/**
 * Build responsivefont_settings_form form.
 *
 * @param array $form_state
 * @return array The created form.
 */
function responsivefont_settings_form($form_state) {
  $form = array();

  $form['responsivefont_always_add_js'] = array(
    '#type' => 'checkbox',
    '#title' => t('Always include JavaScript file to the site.'),
    '#default_value' => variable_get('responsivefont_always_add_js'),
  );
  $form['responsivefont_ignore_admin_theme'] = array(
    '#type' => 'checkbox',
    '#title' => t('Ignore Admin Theme'),
    '#default_value' => variable_get('responsivefont_ignore_admin_theme'),
    '#description' => t('Disable Responsive Fonts in Admin theme.'),
  );

  return system_settings_form($form);
}
