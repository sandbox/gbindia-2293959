(function ($) {
$(document).ready(function(){
$('body').flowtype({
   minimum   : 300,
   maximum   : 1200,
   minFont   : 10,
   maxFont   : 40,
   fontRatio : 30,
   lineRatio : 1.45
});

});
})(jQuery);
